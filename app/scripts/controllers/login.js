'use strict';

/**
 * @ngdoc function
 * @name fhirtestdataApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the fhirtestdataApp
 */
angular.module('fhirtestdataApp')
  .controller('LoginCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
