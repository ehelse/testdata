'use strict';

/**
 * @ngdoc function
 * @name fhirtestdataApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fhirtestdataApp
 */
angular.module('fhirtestdataApp')
  .controller('MainCtrl', function ($scope, $q, $interval) {

    $scope.log = [];

    $scope.data = {
      //baseUrl: window.location.origin
      //baseUrl: 'http://continua.cloudapp.net/baseDstu2',
      baseUrl: 'http://nhn.ehelselab.com/baseDstu2',
      deviceId: '1-5c313efffe018148',
      patientId: 'P0',
      systolic: 120,
      diastolic: 80,
      weight: 80,
      pulse: 70,
      saturation: 96,
      fromDate: new Date(),
      howMany: 5,
      variation: 4
      };


    $scope.createTestData = function() {

      console.log('Data type:'+JSON.stringify($scope.data));

      for (var o = 0; o < $scope.data.howMany; o++) {

        var eventDate = new Date();
        //eventDate.setDate($scope.data.fromDate.getDate() + o);
        eventDate.setTime($scope.data.fromDate.getTime() + (24*3600*1000*o));

        console.log('Observation '+o+'/'+$scope.data.howMany+' start '+$scope.data.fromDate+' now '+eventDate+' getTime:'+eventDate.getTime());

        switch ($scope.data.type) {
          case 'Blood pressure':
            var json = $scope.createBloodPressure($scope.data.patientId,
              eventDate,
              $scope.data.deviceId,
              $scope.randomize($scope.data.systolic, $scope.data.variation),
              $scope.randomize($scope.data.diastolic, $scope.data.variation),
              (2*$scope.data.diastolic/3 + $scope.data.systolic/3) );
            //console.log('$scope.data.example=' + JSON.stringify(json));
            $scope.submitObservation(json);
            break;
          case 'Weight':
            var json = $scope.createWeight($scope.data.patientId,
              eventDate,
              $scope.data.deviceId,
              $scope.randomize($scope.data.weight, $scope.data.variation));
            //console.log('$scope.data.example=' + JSON.stringify(json));
            $scope.submitObservation(json);
            break;
          case 'Pulse':
            var json = $scope.createPulse($scope.data.patientId,
              eventDate,
              $scope.data.deviceId,
              $scope.randomize($scope.data.pulse, $scope.data.variation));
            //console.log('$scope.data.example=' + JSON.stringify(json));
            $scope.submitObservation(json);
            break;
          case 'O2 saturation':
            var json = $scope.createSaturation($scope.data.patientId,
              eventDate,
              $scope.data.deviceId,
              $scope.randomize($scope.data.saturation, $scope.data.variation));
            //console.log('$scope.data.example=' + JSON.stringify(json));
            $scope.submitObservation(json);
            break;
          default:
            alert('No dataType chosen (' + $scope.data.type + ')');
        }
      }
    };

    $scope.randomize = function(value, variation) {
      var random = (Math.random()*2)-1;
      return Math.round(value + (variation*random));
    };

    $scope.viewTestData = function() {

      console.log('Data type:'+JSON.stringify($scope.data));

      switch($scope.data.type) {
        case 'Blood pressure':
          var bp = $scope.createBloodPressure($scope.data.patientId,
            $scope.data.fromDate,
            $scope.data.deviceId,
            $scope.data.systolic,
            $scope.data.diastolic,
            Math.round(2*$scope.data.diastolic/3 + $scope.data.systolic/3) );
          //console.log('BP:'+bp);
          $scope.data.example = bp;
          break;
        case 'Weight':
          var json = $scope.createWeight($scope.data.patientId,
            $scope.data.fromDate,
            $scope.data.deviceId,
            $scope.data.weight);
          //console.log('$scope.data.example=' + JSON.stringify(json));
          $scope.data.example = json;
          break;
        case 'Pulse':
          var json = $scope.createPulse($scope.data.patientId,
            $scope.data.fromDate,
            $scope.data.deviceId,
            $scope.data.pulse);
          //console.log('$scope.data.example=' + JSON.stringify(json));
          $scope.data.example = json;
          break;
        case 'O2 saturation':
          var json = $scope.createSaturation($scope.data.patientId,
            $scope.data.fromDate,
            $scope.data.deviceId,
            $scope.data.saturation);
          //console.log('$scope.data.example=' + JSON.stringify(json));
          $scope.data.example = json;
          break;
        default:
          alert('No dataType chosen ('+$scope.data.type+')');
      }
    };

    $scope.createBloodPressure = function(patientId, effectiveDateTime, deviceId, sys, dia, mean) {

      var r = {};
      r.resourceType = 'Observation';
      r.status = 'final';
      r.category = {
        "coding": [
          {
            "system": "http://hl7.org/fhir/observation-category",
            "code": "vital-signs",
            "display": "Vital Signs"
          }
        ],
        "text": "Vital Signs"
      };
      r.code = {
        "coding": [
          {
            "system": "urn:std:iso:11073:10101",
            "code": "150020",
            "display": "MDC_PRESS_BLD_NONINV"
          }
        ],
        "text":"Blood pressure"
      };
      r.subject = {"reference": "Patient/"+patientId };
      r.effectiveDateTime = effectiveDateTime;
      r.device = { "reference": "Device/"+deviceId };
      r.component = [
        {
          "code": {
            "coding": [
              {
                "system": "urn:std:iso:11073:10101",
                "code": "150021",
                "display": "MDC_PRESS_BLD_NONINV_SYS"
              }
            ]
          },
          "valueQuantity": {
            "value": sys,
            "unit": "mmHg",
            "system": "urn:std.iso:11073:10101",
            "code": "266016"
          }
        },
        {
          "code": {
            "coding": [
              {
                "system": "urn:std:iso:11073:10101",
                "code": "150022",
                "display": "MDC_PRESS_BLD_NONINV_DIA"
              }
            ]
          },
          "valueQuantity": {
            "value": dia,
            "unit": "mmHg",
            "system": "urn:std.iso:11073:10101",
            "code": "266016"
          }
        },
        {
          "code": {
            "coding": [
              {
                "system": "urn:std:iso:11073:10101",
                "code": "150023",
                "display": "MDC_PRESS_BLD_NONINV_MEAN"
              }
            ]
          },
          "valueQuantity": {
            "value": mean,
            "unit": "mmHg",
            "system": "urn:std.iso:11073:10101",
            "code": "266016"
          }
        }
      ];
      return r;
    };

    $scope.createSaturation = function(patientId, effectiveDateTime, deviceId, saturation) {

      var r = {};
      r.resourceType = 'Observation';
      r.status = 'final';
      r.category = {
        "coding": [
          {
            "system": "http://hl7.org/fhir/observation-category",
            "code": "vital-signs",
            "display": "Vital Signs"
          }
        ],
        "text": "Vital Signs"
      };
      r.code = {
        "coding": [
          {
            "system": "urn:std:iso:11073:10101",
            "code": "150456",
            "display": "Oxygen saturation in Arterial blood"
          },
          {
            "system": "http://loinc.org",
            "code": "2708-6",
            "display": "Oxygen saturation in Arterial blood"
          }
        ],
        "text": "oxygen_saturation"
      };
      r.subject = {"reference": "Patient/"+patientId };
      r.effectiveDateTime = effectiveDateTime;
      r.device = { "reference": "Device/"+deviceId };
      r.text = {
        "status": "generated",
        "div": "<div>"+effectiveDateTime+": oxygen_saturation = "+saturation+" %{HemoglobinSaturation}</div>"
      },
      r.valueQuantity = {
        "value": saturation,
        "unit": "%O2",
        "system": "urn:std:iso:11073:10101",
        "code": "262688"
      };

      return r;
    };

    $scope.createPulse = function(patientId, effectiveDateTime, deviceId, pulse) {

      var r = {};
      r.resourceType = 'Observation';
      r.status = 'final';
      r.category = {
        "coding": [
          {
            "system": "http://hl7.org/fhir/observation-category",
            "code": "vital-signs",
            "display": "Vital Signs"
          }
        ],
        "text": "Vital Signs"
      };
      r.code = {
        "coding": [
          {
            "system": "urn:std:iso:11073:10101",
            "code": "149530",
            "display": "heart rate"
          },
          {
            "system": "http://loinc.org",
            "code": "8867-4",
            "display": "heart rate"
          }
        ],
        "text": "heart_rate"
      };
      r.subject = {"reference": "Patient/"+patientId };
      r.effectiveDateTime = effectiveDateTime;
      r.device = { "reference": "Device/"+deviceId };
      r.text = {
        "status": "generated",
        "div": "<div>"+effectiveDateTime+": heart_rate = "+pulse+" bpm</div>"
      },
      r.valueQuantity = {
        "value": pulse,
        "unit": "bpm",
        "system": "urn:std:iso:11073:10101",
        "code": "264864"
      };

      return r;
    };

    $scope.createWeight = function(patientId, effectiveDateTime, deviceId, weight) {

      var r = {};
      r.resourceType = 'Observation';
      r.status = 'final';
      r.category = {
        "coding": [
          {
            "system": "http://hl7.org/fhir/observation-category",
            "code": "vital-signs",
            "display": "Vital Signs"
          }
        ],
        "text": "Vital Signs"
      };
      r.code = {
        "coding": [
          {
            "system": "urn:std:iso:11073:10101",
            "code": "188736",
            "display": "MDC_MASS_BODY_ACTUAL"
          },{
            "system": "http://loinc.org",
            "code": "29463-7",
            "display": "Body Weight"
          },
          {
            "system": "http://loinc.org",
            "code": "3141-9",
            "display": "Body weight Measured"
          },
          {
            "system": "http://snomed.info/sct",
            "code": "27113001",
            "display": "Body weight"
          }
        ]
      };
      r.subject = {"reference": "Patient/"+patientId };
      r.effectiveDateTime = effectiveDateTime;
      r.device = { "reference": "Device/"+deviceId };
      r.valueQuantity = {
        "value": weight,
        "unit": "kg",
        "system": "urn:std.iso:11073:10101",
        "code": "263875"
      };

      return r;

    };

    $scope.submitObservation = function (json) {

      var header = {};
      if ($scope.data.bearerToken != null) {
        header.Authorization = "Bearer "+$scope.data.bearerToken;
        console.log('Adding authorization header');
      }

      $.ajax({
        type: "POST",
        url: $scope.data.baseUrl + '/Observation',
        headers: header,
        async: false,
        data: JSON.stringify(json),
        success: function (data) {
          //console.log('From server:' + JSON.stringify(data));
          /*$scope.$apply(function () {
            $scope.message = 'Data saved to cloud. '+JSON.stringify(data.issue[0].diagnostics);
          });*/
        },
        datatype: 'json',
        contentType: 'application/json+fhir;charset=utf-8'
      }).fail(function (error) {
        console.log('Save error:' + error + ',' + JSON.stringify(error));
        $scope.log.push(data);
      }).done(function (data) {
        //console.log("Returned success from server. Sample of data:" + JSON.stringify(data));
        //$scope.log.push(data);
        $scope.numberOfObservationsCreated = $scope.numberOfObservationsCreated + 1;
      });
    };

    $scope.putPatient = function(json, resourceId) {
      var header = {};
      if ($scope.data.bearerToken != null) {
        header.Authorization = "Bearer "+$scope.data.bearerToken;
        //console.log('Adding authorization header');
      }

      $.ajax({
        type: "PUT",
        url: $scope.data.baseUrl + '/Patient/'+resourceId,
        headers: header,
        async: false,
        data: JSON.stringify(json),
        success: function (data) {
          /*console.log('From server:' + JSON.stringify(data));
          $scope.$apply(function () {
            $scope.message = 'Data saved to cloud. '+JSON.stringify(data.issue[0].diagnostics);
          });*/
        },
        datatype: 'json',
        contentType: 'application/json+fhir;charset=utf-8'
      }).fail(function (error) {
        console.log('Save error:' + error + ',' + JSON.stringify(error));
        $scope.createdFailed = $scope.createdFailed +1;
        //$scope.log.push(data);
      }).done(function (data) {
        $scope.createdCount = $scope.createdCount +1;
        //console.log("Returned success from server. Sample of data:" + JSON.stringify(data));
        //$scope.log.push(data);
      });
    };


    $scope.createPatientJson = function(resourceId, patientId) {
        var json = {
            "resourceType": "Patient",
            "id": resourceId,
            "text": {
                "status": "generated",
                "div": "<div><table class=\"hapiPropertyTable\"><tbody><tr><td>Identifier</td><td>"+patientId+"</td></tr></tbody></table></div>"
            },
            "identifier": [
                {
                    "system": "oid:2.16.578.1.12.4.1.1.8116",
                    "code": "FNR",
                    "value": patientId
                }
            ]
        };
        return json;
    };

    $scope.XcreateTestPatients = function() {
        $scope.createdCount = 0;
        $scope.createdFailed = 0;
        $scope.patientCounter = 0
        $scope.createPatientTask = $interval($scope.createPatient, 100);
    };

    $scope.XcreatePatient = function() {
        console.log('$scope.patientCounter = '+$scope.patientCounter+'/'+$scope.data.howManyPatients);
        var str = "" + $scope.patientCounter
        var pad = "1234500000"
        var patientId = pad.substring(0, pad.length - str.length) + str
        var resourceId = 'P'+$scope.patientCounter;
        var p = $scope.createPatientJson(resourceId, patientId);
        $scope.putPatient(p, resourceId);
        $scope.patientCounter = $scope.patientCounter + 1;
        if ($scope.patientCounter > $scope.data.howManyPatients) {
            $interval.cancel($scope.createPatientTask);
        }
    };

    $scope.createPatient = function(count) {
        console.log('$scope.patientCounter = '+count+'/'+$scope.data.howManyPatients);
        var str = "" + (count+$scope.data.startFrom);
        var pad = "1234500000"
        var patientId = pad.substring(0, pad.length - str.length) + str;
        var resourceId = 'P'+(count+$scope.data.startFrom);
        $scope.data.patientId = resourceId;
        var p = $scope.createPatientJson(resourceId, patientId);
        $scope.putPatient(p, resourceId);
        $scope.patientCounter = $scope.patientCounter + 1;
    };

    $scope.createTestPatients = function() {
        $scope.createdCount = 0;
        $scope.createdFailed = 0;
        $scope.patientCounter = 0;
        $scope.numberOfObservationsCreated = 0;
        for (var i = 0; i < $scope.data.howManyPatients; i++)  {
            $scope.createPatient(i);
            $scope.createTestData();
            console.clear();
        }
        //$scope.createPatientTask = $interval($scope.createPatient, 100);
    };

});
