'use strict';

/**
 * @ngdoc overview
 * @name fhirtestdataApp
 * @description
 * # fhirtestdataApp
 *
 * Main module of the application.
 */
angular
  .module('fhirtestdataApp', [
    'ngAnimate'
  ]);
